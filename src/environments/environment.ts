// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBLG5yQajYB_Pah2EayWr5vhHu_OvoTx74",
    authDomain: "islam-sons.firebaseapp.com",
    databaseURL: "https://islam-sons.firebaseio.com",
    projectId: "islam-sons",
    storageBucket: "islam-sons.appspot.com",
    messagingSenderId: "838778122168",
    appId: "1:838778122168:web:58afd2d1d9dbcbef96294d"
  }
  };
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
