import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

/*CoreUI Modules*/
import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

/* Forms Modules */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Perfect Sidebar module */
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { LaddaModule } from 'angular2-ladda';


/* Mat angular components */
import {MatTabsModule} from '@angular/material/tabs';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';

/*select*/
import { NgSelectModule } from '@ng-select/ng-select';

/* Import 3rd party components */
import { ToastrModule } from 'ngx-toastr';
import { NgxUiLoaderModule, NgxUiLoaderConfig, SPINNER, POSITION, PB_DIRECTION } from 'ngx-ui-loader';
const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsColor: 'red',
  bgsPosition: POSITION.bottomCenter,
  bgsSize: 40,
  bgsType: SPINNER.rectangleBounce, // background spinner type
  fgsType: SPINNER.ballSpinClockwise, // foreground spinner type
  pbDirection: PB_DIRECTION.leftToRight, // progress bar direction
  pbThickness: 5, // progress bar thickness
};
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { DataTablesModule } from 'angular-datatables';

import { ModalModule } from 'ngx-bootstrap';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {NgxPrintModule} from 'ngx-print';

/* JQ-widgets Components*/
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { MainComponent } from './views/main/main.component';
import { HeaderComponent } from './views/core/header/header.component';
import { SideNavComponent } from './views/core/side-nav/side-nav.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { DashboardMainComponent } from './views/dashboard-main/dashboard-main.component';
import { LoginComponent } from './views/login/login.component';
import { NotFoundComponent } from './views/not-found/not-found.component';
import { AuthGuardService } from './services/authGuard/auth-guard.service';
import { LogoutComponent } from './views/logout/logout.component';
import { AddInventoryComponent } from './components/add-inventory/add-inventory.component';
import { RemoveInventoryComponent } from './components/remove-inventory/remove-inventory.component';
import { InventoryHistoryComponent } from './views/inventory-history/inventory-history.component';
import { ProductsComponent } from './views/products/products.component';
import { OrdersComponent } from './views/orders/orders.component';
import { ViewOrdersComponent } from './views/view-orders/view-orders.component';
import { OrderDetailComponent } from './views/order-detail/order-detail.component';
import { MainOrderComponent } from './views/main-order/main-order.component';
import { ProductPopupComponent } from './components/product-popup/product-popup.component';
import { ConfirmationPopupComponent } from './components/confirmation-popup/confirmation-popup.component';
import { VendorsComponent } from './views/vendors/vendors.component';
import { MainVendorComponent } from './views/main-vendor/main-vendor.component';
import { AddVendorPopupComponent } from './components/add-vendor-popup/add-vendor-popup.component';
import { AddProductPopupComponent } from './components/add-product-popup/add-product-popup.component';
import { MainProductComponent } from './views/main-product/main-product.component';
import { ProductDetailComponent } from './views/product-detail/product-detail.component';
import { ViewInventoryRecPopupComponent } from './components/view-inventory-rec-popup/view-inventory-rec-popup.component';
import { VendorDetailComponent } from './views/vendor-detail/vendor-detail.component';
import { ViewOrderDetailPopupComponent } from './components/view-order-detail-popup/view-order-detail-popup.component';
import { PaymentsComponent } from './views/payments/payments.component';
import { NewPaymentComponent } from './views/new-payment/new-payment.component';
import { ViewPaymentPopupComponent } from './components/view-payment-popup/view-payment-popup.component';
import { ViewInvoicePopupComponent } from './components/view-invoice-popup/view-invoice-popup.component';
import { CompanySummaryComponent } from './views/company-summary/company-summary.component';
import { ReloadButtonComponent } from './components/reload-button/reload-button.component';
import { OrdersKanbanBoardComponent } from './views/orders-kanban-board/orders-kanban-board.component';
import { jqxKanbanComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxkanban';
import { InventoryComponent } from './views/inventory/inventory.component';
import { TruckExpenseComponent } from './views/truck-expense/truck-expense.component';
import { ViewTruckExpensesComponent } from './views/view-truck-expenses/view-truck-expenses.component';
import { PrintLogoComponent } from './components/print-logo/print-logo.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HeaderComponent,
    SideNavComponent,
    DashboardComponent,
    DashboardMainComponent,
    LoginComponent,
    NotFoundComponent,
    jqxDateTimeInputComponent,
    LogoutComponent,
    AddInventoryComponent,
    RemoveInventoryComponent,
    InventoryHistoryComponent,
    ProductsComponent,
    OrdersComponent,
    ViewOrdersComponent,
    OrderDetailComponent,
    MainOrderComponent,
    ProductPopupComponent,
    ConfirmationPopupComponent,
    VendorsComponent,
    MainVendorComponent,
    AddVendorPopupComponent,
    AddProductPopupComponent,
    MainProductComponent,
    ProductDetailComponent,
    jqxKanbanComponent,
    ViewInventoryRecPopupComponent,
    VendorDetailComponent,
    ViewOrderDetailPopupComponent,
    PaymentsComponent,
    NewPaymentComponent,
    ViewPaymentPopupComponent,
    ViewInvoicePopupComponent,
    CompanySummaryComponent,
    ReloadButtonComponent,
    OrdersKanbanBoardComponent,
    InventoryComponent,
    TruckExpenseComponent,
    ViewTruckExpensesComponent,
    PrintLogoComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    BrowserAnimationsModule,
    NgSelectModule,
    AppAsideModule,
    ToastrModule.forRoot({
      maxOpened: 1,
      autoDismiss: true
    }),
    PerfectScrollbarModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    MatTabsModule,
    NgxPrintModule,
    HttpClientModule,
    MatFormFieldModule,
    MatChipsModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatExpansionModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatSelectModule,
    MatDialogModule,
    Ng2SearchPipeModule,
    ModalModule.forRoot(),
    AppFooterModule,
    AngularFontAwesomeModule,
    DataTablesModule,
    NgbModule,
    AppSidebarModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    LaddaModule
  ],
  providers: [
    AuthGuardService, { provide: MatDialogRef, useValue: {} }, AngularFireAuth
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ProductPopupComponent,
    AddProductPopupComponent,
    ConfirmationPopupComponent,
    AddVendorPopupComponent,
    ViewInventoryRecPopupComponent,
    ViewOrderDetailPopupComponent,
    ViewPaymentPopupComponent,
    ViewInvoicePopupComponent,
    PrintLogoComponent]
})
export class AppModule { }
