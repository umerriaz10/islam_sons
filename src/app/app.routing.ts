import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './views/main/main.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { DashboardMainComponent } from './views/dashboard-main/dashboard-main.component';
import { LoginComponent } from './views/login/login.component';
import { NotFoundComponent } from './views/not-found/not-found.component';
import { AuthGuardService } from './services/authGuard/auth-guard.service';
import { LogoutComponent } from './views/logout/logout.component';
import { InventoryHistoryComponent } from './views/inventory-history/inventory-history.component';
import { ProductsComponent } from './views/products/products.component';
import { OrdersComponent } from './views/orders/orders.component';
import { ViewOrdersComponent } from './views/view-orders/view-orders.component';
import { OrderDetailComponent } from './views/order-detail/order-detail.component';
import { MainOrderComponent } from './views/main-order/main-order.component';
import { MainVendorComponent } from './views/main-vendor/main-vendor.component';
import { VendorsComponent } from './views/vendors/vendors.component';
import { MainProductComponent } from './views/main-product/main-product.component';
import { ProductDetailComponent } from './views/product-detail/product-detail.component';
import { VendorDetailComponent } from './views/vendor-detail/vendor-detail.component';
import { NewPaymentComponent } from './views/new-payment/new-payment.component';
import { PaymentsComponent } from './views/payments/payments.component';
import { CompanySummaryComponent } from './views/company-summary/company-summary.component';
import { OrdersKanbanBoardComponent } from './views/orders-kanban-board/orders-kanban-board.component';
import { InventoryComponent } from './views/inventory/inventory.component';
import { TruckExpenseComponent } from './views/truck-expense/truck-expense.component';
import { ViewTruckExpensesComponent } from './views/view-truck-expenses/view-truck-expenses.component';


export const routes: Routes = [
  {
    path: 'main',
    component: MainComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        },
        children: [
          {
            path: '',
            component: DashboardMainComponent,
          }
        ]
      },
      {
        path: 'inventory-records',
        component: InventoryHistoryComponent,
        data: {
          title: 'Inventory Record'
        }
      },
      {
        path: 'inventory',
        component: InventoryComponent,
        data: {
          title: 'Inventory'
        }
      },
      {
        path: 'company-summary',
        component: CompanySummaryComponent,
        data: {
          title: 'Company Summary'
        }
      },
      {
        path: 'kanban-board/order-detail/:vendor-name/:id',
        component: OrderDetailComponent,
        data: {
          title: 'Orders Kanban Board'
        }
      },
      {
        path: 'new-order',
        component: OrdersComponent,
        data: {
          title: 'New Order'
        }
      },
      {
        path: 'new-truck-expense',
        component: TruckExpenseComponent,
        data: {
          title: 'Truck Expense'
        }
      },
      {
        path: 'view-orders/:orderType',
        component: ViewOrdersComponent,
        data: {
          title: 'View Order'
        },
        // children: [
        //   {
        //   path: '',
        //   component: ViewOrdersComponent,
        //   },
        //   {
        //     path: 'order-detail/:vendor-name/:id',
        //     component: OrderDetailComponent,
        //   }
        // ]
      },
      {
        path: 'view-truck-expenses/:expenseType',
        component: ViewTruckExpensesComponent,
        data: {
          title: 'View Order'
        },
        // children: [
        //   {
        //   path: '',
        //   component: ViewOrdersComponent,
        //   },
        //   {
        //     path: 'order-detail/:vendor-name/:id',
        //     component: OrderDetailComponent,
        //   }
        // ]
      },
      {
        path: 'new-payment',
        component: NewPaymentComponent,
        data: {
          title: 'New Payment'
        }
      },
      {
        path: 'payments',
        component: PaymentsComponent,
        data: {
          title: 'Payments'
        }
      },
      {
        path: 'products',
        component: MainProductComponent,
        data: {
          title: 'Products'
        },
        children: [
          {
          path: '',
          component: ProductsComponent,
          },
          {
            path: 'product-detail/:product-name/:id',
            component: ProductDetailComponent,
          }
        ]
      },
      {
        path: 'view-vendors',
        component: MainVendorComponent,
        data: {
          title: 'View Vendor'
        },
        children: [
          {
          path: '',
          component: VendorsComponent,
          },
          {
            path: 'vendor-detail/:vendor-name/:id',
            component: VendorDetailComponent,
          }
        ]
      }
    ]
  },
  {
    path: 'main/logout',
    canActivate: [AuthGuardService],
    component: LogoutComponent
  },
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
