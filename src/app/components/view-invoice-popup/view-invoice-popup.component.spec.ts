import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInvoicePopupComponent } from './view-invoice-popup.component';

describe('ViewInvoicePopupComponent', () => {
  let component: ViewInvoicePopupComponent;
  let fixture: ComponentFixture<ViewInvoicePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewInvoicePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewInvoicePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
