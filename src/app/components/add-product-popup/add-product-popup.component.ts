import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { Vendors } from 'src/app/interfaces/vendors';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Order } from 'src/app/interfaces/order';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { TruckExpense } from 'src/app/interfaces/truck-expense';
import { TruckExpenseService } from 'src/app/services/truck-expense/truck-expense.service';

@Component({
  selector: 'app-add-product-popup',
  templateUrl: './add-product-popup.component.html',
  styleUrls: ['./add-product-popup.component.scss']
})
export class AddProductPopupComponent implements OnInit {

/**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof TimeLogDetailComponent
   */
  simpleForm: FormGroup;

  truckExpense: TruckExpense;

  isValid = false;

  submitted = false;

  vendor: Vendors;


  constructor(
    public dialogRef: MatDialogRef<AddProductPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    private truckExpenseService: TruckExpenseService
  ) { }

  ngOnInit() {
    this.createForm();
  }

  /**
   * This method creates the form with all the relevant controls
   *
   * @memberof TimeLogDetailComponent
   */
  createForm() {
    this.truckExpense = this.data.expenseData;
    this.simpleForm = this.formBuilder.group({
      mazda_no: [this.truckExpense.mazda_no, [Validators.required]],
      description: [this.truckExpense.description, [Validators.required]],
      expense: [this.truckExpense.expense, [Validators.required]]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.simpleForm.controls; }

  closeModal() {
    this.dialogRef.close();
  }


  updateExpenseWithFormCheck() {
    this.ngxService.start();
    this.isValid = true;
    this.submitted = true;
    if (this.simpleForm.invalid) {
      this.toastr.error('Please Check Your Form Fields!');
      this.ngxService.stop();
      this.isValid = false;
    }
    if (this.isValid === true) {
      this.updateExpense();
    }
  }

  updateExpense() {
    const truckExpense = this.setValues(this.simpleForm.controls);
    this.truckExpenseService.updateTruckExpense(truckExpense, truckExpense.id).then(res => {
      this.toastr.success('Expense is updated successfully!!');
      this.ngxService.stop();
      this.data.function();
      this.dialogRef.close();
    }).catch(err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
      this.ngxService.stop();
    });
  }

  setValues(form) {
    this.truckExpense.mazda_no = form.mazda_no.value;
    this.truckExpense.description = form.description.value;
    this.truckExpense.expense = form.expense.value;
    return this.truckExpense;
  }

}
