import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { Vendors } from 'src/app/interfaces/vendors';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Order } from 'src/app/interfaces/order';
import { OrdersService } from 'src/app/services/orders/orders.service';

@Component({
  selector: 'app-add-vendor-popup',
  templateUrl: './add-vendor-popup.component.html',
  styleUrls: ['./add-vendor-popup.component.scss']
})
export class AddVendorPopupComponent implements OnInit {

/**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof TimeLogDetailComponent
   */
  simpleForm: FormGroup;

  order: Order;

  isValid = false;

  submitted = false;

  vendor: Vendors;

  constructor(
    public dialogRef: MatDialogRef<AddVendorPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    private orderService: OrdersService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.createForm();
  }

  /**
   * This method creates the form with all the relevant controls
   *
   * @memberof TimeLogDetailComponent
   */
  createForm() {
    this.order = this.data.orderData;
    this.simpleForm = this.formBuilder.group({
      press_name: [this.order.press_name],
      mill_name: [this.order.mill_name],
      mazda_no: [this.order.mazda_no],
      driver_name: [this.order.driver_name],
      labour: [this.order.labour],
      bbc: [this.order.bbc],
      btn: [this.order.btn],
      bleach: [this.order.bleach],
      carug: [this.order.carug],
      news: [this.order.news],
      wc: [this.order.wc],
      proof: [this.order.proof],
      shine: [this.order.shine],
      weightInPress: [this.order.weightInPress],
      weightInMill: [this.order.weightInMill]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.simpleForm.controls; }

  closeModal() {
    this.dialogRef.close();
  }


  updateOrderWithFormCheck() {
    this.ngxService.start();
    this.isValid = true;
    this.submitted = true;
    if (this.simpleForm.invalid) {
      this.toastr.error('Please Correctly fill up the form!');
      this.ngxService.stop();
      this.isValid = false;
    }
    if (this.isValid === true) {
      this.updateOrder();
    }
  }

  updateOrder() {
    const order = this.setValues(this.simpleForm.controls);
    this.orderService.updateOrder(order, order.id).then(res => {
      this.toastr.success('Order is updated successfully!!');
      this.ngxService.stop();
      this.data.function();
      this.dialogRef.close();
    }, err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
      this.ngxService.stop();
    });
  }

  setValues(form) {
    this.order.press_name = form.press_name.value;
    this.order.mill_name = form.mill_name.value;
    this.order.mazda_no = form.mazda_no.value;
    this.order.driver_name = form.driver_name.value;
    this.order.labour = form.labour.value;
    this.order.bbc = form.bbc.value;
    this.order.btn = form.btn.value;
    this.order.bleach = form.bleach.value;
    this.order.carug = form.carug.value;
    this.order.news = form.news.value;
    this.order.wc = form.wc.value;
    this.order.proof = form.proof.value;
    this.order.shine = form.shine.value;
    this.order.weightInMill = form.weightInMill.value;
    this.order.weightInPress = form.weightInPress.value;
    return this.order;
  }

}
