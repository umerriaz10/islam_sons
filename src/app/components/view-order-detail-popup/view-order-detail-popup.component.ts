import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-view-order-detail-popup',
  templateUrl: './view-order-detail-popup.component.html',
  styleUrls: ['./view-order-detail-popup.component.scss']
})
export class ViewOrderDetailPopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ViewOrderDetailPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
  }

  ok() {
    this.dialogRef.close();
  }

}
