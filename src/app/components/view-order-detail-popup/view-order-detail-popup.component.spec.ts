import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOrderDetailPopupComponent } from './view-order-detail-popup.component';

describe('ViewOrderDetailPopupComponent', () => {
  let component: ViewOrderDetailPopupComponent;
  let fixture: ComponentFixture<ViewOrderDetailPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOrderDetailPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOrderDetailPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
