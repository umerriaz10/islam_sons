import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-reload-button',
  templateUrl: './reload-button.component.html',
  styleUrls: ['./reload-button.component.scss']
})
export class ReloadButtonComponent {

  /**
   * This variable is used to store the value of the callback function
   *
   * @memberof ReloadButtonComponent
   */
  @Input() callback: any;

    /**
   * This variable is used to store the value updates
   *
   * @memberof ReloadButtonComponent
   */
  @Input() updates;

  /**
   * The method is used to call the callback function of parent component.
   *
   * @returns
   * @memberof ReloadButtonComponent
   */
  returnCallback() {
    return this.callback();
  }
}
