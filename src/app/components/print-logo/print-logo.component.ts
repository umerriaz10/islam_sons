import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-print-logo',
  templateUrl: './print-logo.component.html',
  styleUrls: ['./print-logo.component.scss']
})
export class PrintLogoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PrintLogoComponent>
  ) { }

  ngOnInit(): void {
  }

  closeModal() {
    this.dialogRef.close();
  }

}
