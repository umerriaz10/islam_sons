import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintLogoComponent } from './print-logo.component';

describe('PrintLogoComponent', () => {
  let component: PrintLogoComponent;
  let fixture: ComponentFixture<PrintLogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintLogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
