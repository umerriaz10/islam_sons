import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInventoryRecPopupComponent } from './view-inventory-rec-popup.component';

describe('ViewInventoryRecPopupComponent', () => {
  let component: ViewInventoryRecPopupComponent;
  let fixture: ComponentFixture<ViewInventoryRecPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewInventoryRecPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewInventoryRecPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
