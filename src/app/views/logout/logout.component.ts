import { Component } from '@angular/core';
import { JwtService } from 'src/app/services/jwt/jwt.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmationPopupComponent } from 'src/app/components/confirmation-popup/confirmation-popup.component';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { VendorsService } from 'src/app/services/vendors/vendors.service';
import { ItemsService } from 'src/app/services/items/items.service';
import { InventoryRecordService } from 'src/app/services/inventory-record/inventory-record.service';
import { PaymentsService } from 'src/app/services/payments/payments.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent {

  /**
   * Object for defining confirmation popup attributes
   *
   * @type {*}
   * @memberof OrderDetailComponent
   */
  confirmationPopupObject: any = {
    'msg': '',
    'cancel': '',
    'confirm': ''
  };


  constructor(
    private jwtService: JwtService,
    private router: Router,
    public matDialog: MatDialog,
    private orderService: OrdersService,
    private vendorService: VendorsService,
    private itemService: ItemsService,
    private invRecService: InventoryRecordService,
    private paymentService: PaymentsService
  ) {
    this.confirmDelete();
  }

    /**
   * This method calls the delete order confirmation popup
   *
   * @memberof OrderDetailComponent
   */
  confirmDelete() {
    this.confirmationPopupObject = {
      'msg': 'Are you sure you want to LOGOUT?',
      'cancel': 'Cancel', 'confirm': 'Logout'
    };
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.confirmationPopupObject;
    const modalDialog = this.matDialog.open(ConfirmationPopupComponent, dialogConfig);
    modalDialog.afterClosed().subscribe(data => {
      if (data) {
        this.logout();
      } else {
        this.router.navigate(['main/dashboard']);
      }
    });
  }

  logout() {
    this.jwtService.destroyToken();
    this.orderService.saveOrdersLocalStorage(null);
    this.vendorService.saveVendorsLocalStorage(null);
    this.itemService.saveItemsLocalStorage(null);
    this.invRecService.saveInvRecordsLocalStorage(null);
    this.paymentService.savePaymentsLocalStorage(null);
    this.router.navigate(['']);
  }

}
