import { Component, OnDestroy, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from 'src/app/interfaces/side-nav';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnDestroy {

/**
 * This property comprises the navItems meta.
 */
 public navItems = navItems;

/**
 * This flag indicates wether the sidebar is minimized or not.
 */
public sidebarMinimized = true;

/**
 * This property is an eventlistener.
 */
private changes: MutationObserver;

/**
 * This property contains the document body.
 */
 public element: HTMLElement;

/**
 * The constructor initializes the navItems meta.
 *
 * This method observes the changes in the DOM, and minimizes and maximizes the
 * side nav accordingly.
 */
 constructor(@Inject(DOCUMENT) _document?: any) {

  this.changes = new MutationObserver((mutations) => {
    this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
  });
  this.element = _document.body;
  this.changes.observe(<Element>this.element, {
    attributes: true,
    attributeFilter: ['class']
  });
}

/**
 * Detaches this view from the change-detection tree.
 */
ngOnDestroy(): void {
  this.changes.disconnect();
  }

}
