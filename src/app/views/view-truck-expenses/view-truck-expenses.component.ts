import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import * as $ from 'jquery';
import 'datatables.net';
import {Overlay} from '@angular/cdk/overlay';
import { formatDate } from '@angular/common';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationPopupComponent } from 'src/app/components/confirmation-popup/confirmation-popup.component';
import { TruckExpense } from 'src/app/interfaces/truck-expense';
import { TruckExpenseService } from 'src/app/services/truck-expense/truck-expense.service';
import { AddProductPopupComponent } from 'src/app/components/add-product-popup/add-product-popup.component';
import { ViewPaymentPopupComponent } from 'src/app/components/view-payment-popup/view-payment-popup.component';


@Component({
  selector: 'app-view-truck-expenses',
  templateUrl: './view-truck-expenses.component.html',
  styleUrls: ['./view-truck-expenses.component.scss']
})
export class ViewTruckExpensesComponent implements OnInit {

  /**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof RegisterComponent
   */
  simpleForm: FormGroup;

 /**
   * Holds the items coming from DB
   *
   * @type {Array<Items>}
   * @memberof ProductsComponent
   */
  truckExpenses: Array<TruckExpense> = [];

  @ViewChild('dataTable', { static: true }) table;

  /**
   * This variable is used to hold jqxDateTimeInput object
   *
   * @type {jqxDateTimeInputComponent}
   * @memberof AddInventoryComponent
   */
  @ViewChild('myDateInput', {static: false}) myDateInput: jqxDateTimeInputComponent;

  /**
   * Holds the datatable object
   *
   * @type {*}
   * @memberof ProductsComponent
   */
  dataTable: any;

  /**
   * Holds the date that user selects
   *
   * @memberof OrderDetailComponent
   */
  selectedDate = formatDate(new Date(), 'MM-yyyy', 'en');

    /**
   * Object for defining confirmation popup attributes
   *
   * @type {*}
   * @memberof OrderDetailComponent
   */
  confirmationPopupObject: any = {
    'msg': '',
    'cancel': '',
    'confirm': ''
  };

  /**
   * Holds the datatable options
   *
   * @memberof ProductsComponent
   */
  dtOption = {
    'paging': false,
    'ordering': true,
    'info': false
  };

  tableRow;

  isAll = false;

  isMazdaNo = false;

  isFirsttime = false;

  isMazdaDatatable = false;

  todaysDate = formatDate(new Date(), 'yyyy/MM/dd', 'en');

  /**
   * Creates an instance of ProductsComponent.
   *
   * @param {ItemsService} itemService
   * @param {NgxUiLoaderService} ngxService
   * @memberof ProductsComponent
   */
  constructor(
    private truckExpenseService: TruckExpenseService,
    private ngxService: NgxUiLoaderService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public overlay: Overlay,
    private router: Router,
    private route: ActivatedRoute,
    public matDialog: MatDialog
  ) {
    this.createForm();
  }

  /**
   * This is the hook called on the initialization of the component, it initializes
   * the form.
   *
   * @memberof ProductsComponent
   */
  ngOnInit() {
    this.createForm();
    this.route.params.subscribe(params => {
      if (params.expenseType === 'All') {
        this.isAll = true;
        this.dataTable = $(this.table.nativeElement);
      } else {
        this.toastr.success('Please Enter Mazda No');
        this.dataTable = $(this.table.nativeElement);
      }
    });
    if (this.isAll === true) {
      this.getTruckExpenses();
    }
  }

  /**
   * This method creates the form with all the relevant controls and validations.
   *
   * @memberof RegisterComponent
   */
  createForm() {
    this.simpleForm = this.formBuilder.group({
      mazda_no: ['', [Validators.required]]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.simpleForm.controls; }

  /**
   * Method to get the Orders
   *
   * @memberof ViewOrdersComponent
   */
  getTruckExpenses() {
    this.ngxService.start();
    this.truckExpenses = [];
    const truckExpenses = this.truckExpenseService.getTruckExpensesFromLocal();
    if (truckExpenses === undefined || truckExpenses === null) {
      this.truckExpenseService.getTruckExpenses().subscribe(res => {
        res.forEach(tExpense => {
          const ord = tExpense.data();
          ord.id = tExpense.id;
          const date = this.sanitizeDate(ord.date);
          ord.custom_date = formatDate(date, 'MM-yyyy', 'en');
          this.truckExpenses.push(ord);
        });
        this.truckExpenseService.saveTruckExpensesLocalStorage(this.truckExpenses);
        this.filtertruckExpenses();
      }, err => {
        this.toastr.error(err);
      });
    } else {
      this.truckExpenses = truckExpenses;
      this.filtertruckExpenses();
    }
  }

  sanitizeDate(date) {
    let orderDate = [];
    orderDate = date.split(',');
    orderDate = orderDate[0].split('-');
    return new Date(orderDate[2] + '-' + orderDate[1] + '-' + orderDate[0]);
  }

  /**
   * Method called on refresh button
   *
   * @memberof ViewOrdersComponent
   */
  refreshRecords () {
    this.truckExpenseService.saveTruckExpensesLocalStorage(null);
    this.dataTable.DataTable().clear().destroy();
    this.dataTable = $(this.table.nativeElement);
    this.getTruckExpenses();
  }

  filtertruckExpenses() {
    if (this.isAll === false) {
      const col = [
        { 'title': 'Date',
          'data': 'date' },
        { 'title': 'Description',
          'data': 'description',
        'defaultContent': 0},
        { 'title': 'Expense',
          'data': 'expense',
        'defaultContent': 0},
        { 'title': 'Actions',
          'data': null,
        'defaultContent': '<i id="edit" class="fa fa-2x fa-eye" style="padding-right: 5px;color: #29557d!important""></i><i id="del" style="color: #29557d!important"" class="fa fa-2x fa-trash"></i>'}
      ];
      this.truckExpenses = _.filter(this.truckExpenses, obj => obj.custom_date === this.selectedDate && obj.mazda_no === this.simpleForm.value.mazda_no );
      if (this.truckExpenses.length > 0) {
        this.isFirsttime = true;
        this.setDataTable(col);
      } else {
      this.toastr.warning('Please recheck Date and Mazda Number');
      this.ngxService.stop();
    }
  } else {
    const col = [
      { 'title': 'Date',
        'data': 'date' },
      { 'title': 'Mazda No',
        'data': 'mazda_no',
      'defaultContent': '-' },
      { 'title': 'Description',
        'data': 'description',
      'defaultContent': 0},
      { 'title': 'Expense',
        'data': 'expense',
      'defaultContent': 0},
      { 'title': 'Actions',
        'data': null,
      'defaultContent': '<i id="edit" class="fa fa-2x fa-eye" style="padding-right: 5px;color: #29557d!important""></i><i id="del" style="color: #29557d!important"" class="fa fa-2x fa-trash"></i>'}
    ];
    if (this.truckExpenses.length > 0) {
      this.setDataTable(col);
    } else {
      this.toastr.warning('No Expenses');
      this.ngxService.stop();
    }
  }
  }

  checkMazdaNo() {
    if (this.simpleForm.valid) {
      this.isMazdaNo = true;
    } else {
      this.isMazdaNo = false;
    }
  }

  /**
   * Sets the data table for products
   *
   * @param {*} vendors
   * @memberof ProductsComponent
   */
  setDataTable(col) {
    this.tableRow = this.dataTable.DataTable({
      dtOption: this.dtOption,
      data: this.truckExpenses,
      columns: col,
      order: [[ 0, 'desc']],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page',
      }});
      this.btnMethods();
    this.ngxService.stop();
  }

  btnMethods() {
    const self = this;
   $(self.dataTable).on( 'click', 'i', function () {
     const id = this.id;
     const data = self.tableRow.row( $(this).parents('tr') ).data();
     if (id === 'del') {
      self.confirmDelete(data);
     } else {
      self.openAddExpensePopup(data);
     }
  });
 }

   /**
   * This method calls the delete order confirmation popup
   *
   * @memberof OrderDetailComponent
   */
  confirmDelete(truckExpense) {
    this.confirmationPopupObject = {
      'msg': 'Are you sure you want to delete this Expense?',
      'cancel': 'Cancel', 'confirm': 'Delete'
    };
    const dialogConfig = new MatDialogConfig();
    dialogConfig.scrollStrategy = this.overlay.scrollStrategies.noop();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.confirmationPopupObject;
    const modalDialog = this.matDialog.open(ConfirmationPopupComponent, dialogConfig);
    modalDialog.afterClosed().subscribe(data => {
      if (data) {
        this.deleteTruckExpense(truckExpense);
      }
    });
  }

  /**
   * This method used to delete the order by also deleting the related order items
   *
   * @memberof OrderDetailComponent
   */
  deleteTruckExpense(truckExpense) {
    this.ngxService.start();
    this.truckExpenseService.removeTruckExpense(truckExpense.id).then(res => {
      this.toastr.success('Expense has been Deleted succesfully!');
      this.refreshRecords();
    }).catch(err => {
      this.ngxService.stop();
      this.toastr.error(err);
    });
  }

  /**
   * Method that opens the Add a product popup
   *
   * @memberof OrderDetailComponent
   */
  openAddExpensePopup(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.scrollStrategy = this.overlay.scrollStrategies.noop();
    dialogConfig.id = 'modal-component';
    dialogConfig.height = 'auto';
    dialogConfig.width = 'auto';
    dialogConfig.data = {'function': this.refreshRecords.bind(this),
                         'expenseData': data};
    const modalDialog = this.matDialog.open(AddProductPopupComponent, dialogConfig);
  }

  /**
   * Method that opens the Add a product popup
   *
   * @memberof OrderDetailComponent
   */
  openExpensesViewPopup() {
    if (this.truckExpenses.length <= 0) {
      this.toastr.warning('There are no Expenses');
    } else {
      const date = formatDate(this.truckExpenses[0].date, 'MM/yyyy', 'en');
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.scrollStrategy = this.overlay.scrollStrategies.noop();
      dialogConfig.id = 'modal-component';
      dialogConfig.height = '80vh';
      dialogConfig.width = 'max-content';
      dialogConfig.data = {'expenses': this.truckExpenses, 'date': date, 'mazda_no': this.truckExpenses[0].mazda_no};
      const modalDialog = this.matDialog.open(ViewPaymentPopupComponent, dialogConfig);
    }
  }

  getExpenses() {
    if (this.isFirsttime === true) {
      this.dataTable.DataTable().clear().destroy();
    }
    this.dataTable = $(this.table.nativeElement);
    this.getTruckExpenses();
  }

}
