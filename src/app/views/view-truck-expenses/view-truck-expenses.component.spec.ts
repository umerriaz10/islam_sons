import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTruckExpensesComponent } from './view-truck-expenses.component';

describe('ViewTruckExpensesComponent', () => {
  let component: ViewTruckExpensesComponent;
  let fixture: ComponentFixture<ViewTruckExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTruckExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTruckExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
