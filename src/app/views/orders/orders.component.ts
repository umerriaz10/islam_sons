import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { Order } from 'src/app/interfaces/order';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user/user.service';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  /**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof RegisterComponent
   */
  simpleForm: FormGroup;

  /**
   * This variable is used to hold jqxDateTimeInput object
   *
   * @type {jqxDateTimeInputComponent}
   * @memberof AddInventoryComponent
   */
  @ViewChild('myDateInput', {static: false}) myDateInput: jqxDateTimeInputComponent;

  /**
   * This variable is used to set the max date in the datepicker
   *
   * @memberof OrderDetailComponent
   */
  maxDate = new Date();

  /**
   * Holds the date that user selects
   *
   * @memberof OrderDetailComponent
   */
  selectedDate = new Date();

  /**
   * This object is used to hold the order values
   *
   * @type {<any>}
   * @memberof RegisterComponent
   */
  order: Order = {};

  isValid = false;

  submitted = false;

  isQuanErr = false;

  quanErrMsg = '';

  priceErrMsg = '';

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private orderService: OrdersService,
    private userService: UserService,
    private ngxService: NgxUiLoaderService
  ) {
    this.createForm();
  }

  /**
   * Get Called when component initialized for the first time
   *
   * @memberof OrdersComponent
   */
  ngOnInit() {
    this.createForm();
    // this.getProducts();
    // this.getvendors();
  }

  /**
   * This method creates the form with all the relevant controls and validations.
   *
   * @memberof RegisterComponent
   */
  createForm() {
    this.simpleForm = this.formBuilder.group({
      press_name: [''],
      mill_name: [''],
      mazda_no: [''],
      driver_name: [''],
      labour: [''],
      bbc: [''],
      btn: [''],
      bleach: [''],
      carug: [''],
      news: [''],
      wc: [''],
      proof: [''],
      shine: [''],
      weightInPress: [''],
      weightInMill: ['']
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.simpleForm.controls; }

  /**
   * Method that places the order
   *
   * @memberof OrdersComponent
   */
  placeOrder() {
    this.orderService.postOrder(this.order).then(res => {
      this.simpleForm.reset();
      this.orderService.saveOrdersLocalStorage(null);
      this.toastr.success('Congrats! Order has been placed Successfully');
      this.ngxService.stop();
    }).catch(err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
    });
  }

  /**
   * Method that checks the form for errors
   *
   * @memberof OrdersComponent
   */
  checkFormValidation() {
    this.ngxService.start();
    this.isValid = true;
    this.submitted = true;
    if (this.simpleForm.invalid) {
      this.toastr.error('Please Check Your Form Fields!');
      this.ngxService.stop();
      this.isValid = false;
    }
    if (this.isValid === true) {
      this.setValues();
    }
  }

  /**
   * Method that set the order object with required values
   *
   * @memberof OrdersComponent
   */
  setValues() {
    const form = this.simpleForm.controls;
    this.order.date = this.selectedDate;
    this.order.orderedBy = this.userService.getUser().name;
    this.order.press_name = form.press_name.value;
    this.order.mill_name = form.mill_name.value;
    this.order.mazda_no = form.mazda_no.value;
    this.order.driver_name = form.driver_name.value;
    this.order.labour = form.labour.value;
    this.order.bbc = form.bbc.value;
    this.order.btn = form.btn.value;
    this.order.bleach = form.bleach.value;
    this.order.carug = form.carug.value;
    this.order.news = form.news.value;
    this.order.wc = form.wc.value;
    this.order.proof = form.proof.value;
    this.order.shine = form.shine.value;
    this.order.weightInMill = form.weightInMill.value;
    this.order.weightInPress = form.weightInPress.value;
    this.placeOrder();
  }

}
