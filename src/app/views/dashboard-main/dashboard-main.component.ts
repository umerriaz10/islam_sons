import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy} from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import {Overlay} from '@angular/cdk/overlay';
import { PrintLogoComponent } from 'src/app/components/print-logo/print-logo.component';

@Component({
  selector: 'app-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.scss']
})
export class DashboardMainComponent {

  constructor(
    public matDialog: MatDialog,
    public overlay: Overlay,
  ) { }

  printLogo() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.height = '80vh';
    dialogConfig.width = 'auto';
    dialogConfig.scrollStrategy = this.overlay.scrollStrategies.noop();
    const modalDialog = this.matDialog.open(PrintLogoComponent, dialogConfig);
  }

}
