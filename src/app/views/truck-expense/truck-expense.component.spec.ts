import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TruckExpenseComponent } from './truck-expense.component';

describe('TruckExpenseComponent', () => {
  let component: TruckExpenseComponent;
  let fixture: ComponentFixture<TruckExpenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TruckExpenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TruckExpenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
