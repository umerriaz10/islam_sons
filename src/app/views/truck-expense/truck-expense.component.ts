import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import { formatDate } from '@angular/common';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserService } from 'src/app/services/user/user.service';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { TruckExpense } from 'src/app/interfaces/truck-expense';
import { TruckExpenseService } from 'src/app/services/truck-expense/truck-expense.service';


@Component({
  selector: 'app-truck-expense',
  templateUrl: './truck-expense.component.html',
  styleUrls: ['./truck-expense.component.scss']
})
export class TruckExpenseComponent implements OnInit {
/**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof RegisterComponent
   */
  simpleForm: FormGroup;

  /**
   * This variable is used to hold jqxDateTimeInput object
   *
   * @type {jqxDateTimeInputComponent}
   * @memberof AddInventoryComponent
   */
  @ViewChild('myDateInput', {static: false}) myDateInput: jqxDateTimeInputComponent;

  /**
   * This variable is used to set the max date in the datepicker
   *
   * @memberof OrderDetailComponent
   */
  maxDate = new Date();

  /**
   * Holds the date that user selects
   *
   * @memberof OrderDetailComponent
   */
  selectedDate = new Date();

  /**
   * This object is used to hold the order values
   *
   * @type {<any>}
   * @memberof RegisterComponent
   */
  truckExpense: TruckExpense = {};

  isValid = false;

  submitted = false;

  isQuanErr = false;

  quanErrMsg = '';

  priceErrMsg = '';

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private truckExpenseService: TruckExpenseService,
    private userService: UserService,
    private ngxService: NgxUiLoaderService
  ) {
    this.createForm();
  }

  /**
   * Get Called when component initialized for the first time
   *
   * @memberof OrdersComponent
   */
  ngOnInit() {
    // this.getProducts();
    // this.getvendors();
  }

  /**
   * This method creates the form with all the relevant controls and validations.
   *
   * @memberof RegisterComponent
   */
  createForm() {
    this.simpleForm = this.formBuilder.group({
      mazda_no: ['', [Validators.required]],
      description: ['', [Validators.required]],
      expense: ['', [Validators.required]]
    });
  }

  /**
   * This method is convenience getter for easy access to form fields
   *
   * @memberof AddInventoryComponent
   */
  get f() { return this.simpleForm.controls; }

  /**
   * Method that places the order
   *
   * @memberof OrdersComponent
   */
  addTruckExpense() {
    this.truckExpenseService.postTruckExpense(this.truckExpense).then(res => {
      this.simpleForm.reset();
      this.truckExpenseService.saveTruckExpensesLocalStorage(null);
      this.toastr.success('Congrats! Expense has been added Successfully');
      this.ngxService.stop();
    }).catch(err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
    });
  }

  /**
   * Method that checks the form for errors
   *
   * @memberof OrdersComponent
   */
  checkFormValidation() {
    this.ngxService.start();
    this.isValid = true;
    this.submitted = true;
    if (this.simpleForm.invalid) {
      this.toastr.error('Please Check Your Form Fields!');
      this.ngxService.stop();
      this.isValid = false;
    }
    if (this.isValid === true) {
      this.setValues();
    }
  }

  /**
   * Method that set the order object with required values
   *
   * @memberof OrdersComponent
   */
  setValues() {
    const form = this.simpleForm.controls;
    const date = formatDate(new Date(this.selectedDate), 'dd-MM-yyyy, h:mm a E', 'en');
    this.truckExpense.date = date;
    this.truckExpense.addedBy = this.userService.getUser().name;
    this.truckExpense.mazda_no = form.mazda_no.value;
    this.truckExpense.description = form.description.value;
    this.truckExpense.expense = form.expense.value;
    this.addTruckExpense();
  }
}
