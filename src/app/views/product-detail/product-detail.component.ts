import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { ItemsService } from 'src/app/services/items/items.service';
import { ConfirmationPopupComponent } from 'src/app/components/confirmation-popup/confirmation-popup.component';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

   /**
   * Variable that holds the order object
   *
   * @type {*}
   * @memberof OrderDetailComponent
   */
  product: any;

  /**
   * Flag to switch between edit and view mode
   *
   * @memberof OrderDetailComponent
   */
  isEdit = false;

  /**
   * Object for defining confirmation popup attributes
   *
   * @type {*}
   * @memberof OrderDetailComponent
   */
  confirmationPopupObject: any = {
    'msg': '',
    'cancel': '',
    'confirm': ''
  };

  /**
   * This property is the form group element for holding the data.
   *
   * @type {FormGroup}
   * @memberof OrderDetailComponent
   */
  productForm: FormGroup;

  /**
   * Flag that indicates whether the order is changed or not
   *
   * @memberof OrderDetailComponent
   */
  isChangeFlag = false;

  submitted = true;

  isValid = false;

  proId;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private ngxService: NgxUiLoaderService,
    private itemService: ItemsService,
    private formBuilder: FormBuilder,
    public matDialog: MatDialog
  ) { }

  /**
   * This is the hook called on the initialization of the component, it initializes
   * the form.
   *
   * @memberof OrderDetailComponent
   */
  ngOnInit() {
    this.ngxService.start();
    this.route.params.subscribe(params => {
      this.getProductById(params.id);
    });
  }

  /**
   * This method is used to get the main order on the basis of id
   *
   * @param {*} id
   * @memberof OrderDetailComponent
   */
  getProductById(id) {
    this.itemService.getProductById(id).subscribe(response => {
      this.product = response.data();
      this.proId = id;
      this.createForm();
    }, err => {
      this.errorGettingOrders(err);
    });
  }

  /**
   * Method to be called when API causes error in getting orders
   *
   * @param {*} err
   * @memberof OrderDetailComponent
   */
  errorGettingOrders(err) {
    this.toastr.error(err);
    this.router.navigate(['/main/products/']);
    this.ngxService.stop();
  }

  /**
   * This method creates the form with all the relevant controls
   *
   * @memberof OrderDetailComponent
   */
  createForm() {
    this.productForm = this.formBuilder.group({
      p_name: [this.product.p_name, Validators.required],
      p_quantity: [this.product.p_quantity, Validators.required],
      p_price: [this.product.p_price, Validators.required]
    });
    this.productForm.disable();
    this.ngxService.stop();
  }

  get f() { return this.productForm.controls; }

  /**
   * This method allows to toggle between edit mode, disable some fields
   *
   * @memberof OrderDetailComponent
   */
  onEditBtn() {
    this.isEdit = !this.isEdit;
    this.productForm.enable();
  }

  /**
   * This method allows to toggle between edit mode, disable some fields
   *
   * @memberof OrderDetailComponent
   */
  onCancelBtn() {
    this.isEdit = !this.isEdit;
    this.productForm.disable();
  }

  /**
   * This method calls the delete order confirmation popup
   *
   * @memberof OrderDetailComponent
   */
  confirmDelete() {
    this.confirmationPopupObject = {
      'msg': 'Are you sure you want to delete this Product?',
      'cancel': 'Cancel', 'confirm': 'Delete'
    };
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.confirmationPopupObject;
    const modalDialog = this.matDialog.open(ConfirmationPopupComponent, dialogConfig);
    modalDialog.afterClosed().subscribe(data => {
      if (data) {
        this.deleteProduct();
      }
    });
  }

  /**
   * This method used to delete the order by also deleting the related order items
   *
   * @memberof OrderDetailComponent
   */
  deleteProduct() {
    this.ngxService.start();
    this.itemService.removeProduct(this.proId).then(res => {
      this.itemService.saveItemsLocalStorage(null);
      this.successMessageDisplay('Product has been deleted succesfully!');
    }).catch(err => {
      this.ngxService.stop();
      this.toastr.error(err);
    });
  }

  /**
   * This method is used to display message for successful entry and
   * reroutes the user to orders list view
   *
   * @memberof OrderDetailComponent
   */
  successMessageDisplay(msg) {
    this.toastr.success(msg);
    this.ngxService.stop();
    this.router.navigate(['/main/products/']);
  }

  /**
   * Method that checks the emptiness of order items in an order
   *
   * @memberof OrderDetailComponent
   */
  detectFormChanges() {
    this.ngxService.start();
    this.isValid = true;
    this.submitted = true;
    if (this.productForm.dirty === false) {
      this.isValid = false;
      this.toastr.warning('Please apply some changes!');
      this.ngxService.stop();
    } else if (this.productForm.invalid) {
      this.toastr.error('Please Check Your Form Fields!');
      this.isValid = false;
      this.ngxService.stop();
    } else if (this.isValid === true) {
      this.updateProduct();
    }
  }

  updateProduct() {
    this.setProduct();
    this.itemService.updateItem(this.product, this.proId).then(res => {
      this.successMessageDisplay('Product is successfully updated!');
    }).catch(err => {
      this.toastr.error('Please repeat your action due to this error: ' + err);
      this.ngxService.stop();
    });
  }

  setProduct() {
    this.product.p_name = this.productForm.get('p_name').value;
    this.product.p_quantity = this.productForm.get('p_quantity').value;
    this.product.p_price = this.productForm.get('p_price').value;
  }

}
