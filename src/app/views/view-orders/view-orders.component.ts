import { Component, OnInit, ViewChild } from '@angular/core';
import { Order } from 'src/app/interfaces/order';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'lodash';
import * as $ from 'jquery';
import 'datatables.net';
import {Overlay} from '@angular/cdk/overlay';
import { formatDate } from '@angular/common';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { ConfirmationPopupComponent } from 'src/app/components/confirmation-popup/confirmation-popup.component';
import { AddVendorPopupComponent } from 'src/app/components/add-vendor-popup/add-vendor-popup.component';
import { ViewInvoicePopupComponent } from 'src/app/components/view-invoice-popup/view-invoice-popup.component';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';

@Component({
  selector: 'app-view-orders',
  templateUrl: './view-orders.component.html',
  styleUrls: ['./view-orders.component.scss']
})
export class ViewOrdersComponent implements OnInit {

 /**
   * Holds the items coming from DB
   *
   * @type {Array<Items>}
   * @memberof ProductsComponent
   */
  orders: Array<Order> = [];

  /**
   * Holds the items for data table
   *
   * @type {Array<Items>}
   * @memberof ProductsComponent
   */
  ordersDatatable: Array<Order> = [];

  @ViewChild('dataTable', { static: true }) table;

  /**
   * Holds the datatable object
   *
   * @type {*}
   * @memberof ProductsComponent
   */
  dataTable: any;

/**
   * This variable is used to hold jqxDateTimeInput object
   *
   * @type {jqxDateTimeInputComponent}
   * @memberof AddInventoryComponent
   */
  @ViewChild('myDateInput', {static: false}) myDateInput: jqxDateTimeInputComponent;

    /**
   * Object for defining confirmation popup attributes
   *
   * @type {*}
   * @memberof OrderDetailComponent
   */
  confirmationPopupObject: any = {
    'msg': '',
    'cancel': '',
    'confirm': ''
  };

  /**
   * Holds the datatable options
   *
   * @memberof ProductsComponent
   */
  dtOption = {
    'paging': false,
    'ordering': true,
    'info': false
  };

  tableRow;

  isAll = false;

  isTimeOut = false;

  setId;

  isDateChanged = false;

  selectedDate;

  todaysDate = formatDate(new Date(), 'yyyy/MM/dd', 'en');

  /**
   * Creates an instance of ProductsComponent.
   *
   * @param {ItemsService} itemService
   * @param {NgxUiLoaderService} ngxService
   * @memberof ProductsComponent
   */
  constructor(
    private orderService: OrdersService,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    public overlay: Overlay,
    private router: Router,
    private route: ActivatedRoute,
    public matDialog: MatDialog
  ) { }

  /**
   * This is the hook called on the initialization of the component, it initializes
   * the form.
   *
   * @memberof ProductsComponent
   */
  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.orderType === 'All') {
        this.isAll = true;
      }
    });
    this.getOrders();
  }

  rerenderTable() {
    this.setId = setTimeout(() => {
      const firstColumn = document.querySelectorAll('.colWidth');
      firstColumn.forEach(function(userItem) {
        userItem.setAttribute('style', 'white-space: nowrap !important;');
      });
      const table = $('#dataTable').DataTable();
      table.columns.adjust().draw();
      this.isTimeOut = true;
    }, 1000);
    if (this.isTimeOut === true) {
      clearTimeout(this.setId);
    }
  }

  /**
   * Method to get the Orders
   *
   * @memberof ViewOrdersComponent
   */
  getOrders() {
    this.dataTable = $(this.table.nativeElement);
    this.ngxService.start();
    this.orders = [];
    const orders = this.orderService.getOrdersFromLocal();
    if (orders === undefined || orders === null) {
      this.orderService.getOrders().subscribe(res => {
        res.forEach(order => {
          const ord = order.data();
          ord.id = order.id;
          if (ord.date) {
            const date1 = Date.parse(ord.date);
            ord.custom_date = formatDate(date1, 'yyyy/MM/dd', 'en');
          }
          // const date1 = Date.parse(ord.date);
          // ord.custom_date = formatDate(date1, 'yyyy/MM/dd', 'en');
          this.orders.push(ord);
        });
        this.orderService.saveOrdersLocalStorage(this.orders);
        this.filterOrders();
        this.setDataTable();
      }, err => {
        this.toastr.error(err);
      });
    } else {
      this.orders = orders;
      this.filterOrders();
      this.setDataTable();
    }
  }

  sanitizeDate(date) {
    let orderDate = [];
    orderDate = date.split(',');
    orderDate = orderDate[0].split('-');
    return new Date(orderDate[2] + '-' + orderDate[1] + '-' + orderDate[0]);
  }

  /**
   * Method called on refresh button
   *
   * @memberof ViewOrdersComponent
   */
  refreshRecords () {
    this.orderService.saveOrdersLocalStorage(null);
    this.dataTable.DataTable().clear().destroy();
    this.isTimeOut = false;
    this.getOrders();
  }

  filterOrders() {
    if (this.isAll === false && this.isDateChanged === false) {
      this.orders = _.filter(this.orders, obj => obj.custom_date === this.todaysDate);
    } else if (this.isDateChanged === true) {
      this.orders =  _.filter(this.orders, obj => obj.custom_date === this.selectedDate);
    }
  }

  /**
   * Sets the data table for products
   *
   * @param {*} vendors
   * @memberof ProductsComponent
   */
  setDataTable() {
    this.ordersDatatable = [];
    this.tableRow = this.dataTable.DataTable({
      dtOption: this.dtOption,
      data: this.orders,
      paging: true,
      fixedColumns : false,
      columnDefs: [
        { 'orderable': false, 'targets': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16] }
      ],
      columns: [
        { 'data': 'custom_date',
        'className': 'colWidth'},
        { 'data': 'press_name',
        'defaultContent': '-',
        'className': 'colWidth'},
        { 'data': 'mill_name',
        'className': 'colWidth',
        'defaultContent': 0},
        { 'data': 'mazda_no',
        'defaultContent': 0},
        { 'data': 'driver_name',
        'defaultContent': 0},
        { 'data': 'labour',
        'className': 'colWidth'},
        { 'data': 'bbc',
        'defaultContent': '-' },
        { 'data': 'btn',
        'defaultContent': '-' },
        { 'data': 'bleach',
        'defaultContent': '-' },
        { 'data': 'carug',
        'defaultContent': '-' },
        { 'data': 'news',
        'defaultContent': '-' },
        { 'data': 'wc',
        'defaultContent': '-' },
        { 'data': 'proof',
        'defaultContent': '-' },
        { 'data': 'shine',
        'defaultContent': '-' },
        { 'data': 'weightInMill',
        'defaultContent': '-' },
        { 'data': 'weightInPress',
        'defaultContent': '-' },
        { 'data': null,
        'defaultContent': '<i id="edit" class="fa fa-2x fa-eye" style="padding-right: 5px;color: #29557d!important"></i><i id="del" style="color: #29557d!important" class="fa fa-2x fa-trash"></i>'}
      ],
      order: [[ 0, 'desc']],
      responsive: true,
      autoWidth: true,
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page',
      }});
      this.btnMethods();
    this.ngxService.stop();
    this.rerenderTable();
  }

  btnMethods() {
    const self = this;
   $(self.dataTable).on( 'click', 'i', function () {
     const id = this.id;
     let tr = $(this).closest('tr');
     if (tr.hasClass('child')) {
        tr = $(tr).prev();
     }
     const data = self.tableRow.row(tr).data();
     if (id === 'del') {
      self.confirmDelete(data);
     } else {
      self.openAddVendorPopup(data);
     }
  });
 }

   /**
   * This method calls the delete order confirmation popup
   *
   * @memberof OrderDetailComponent
   */
  confirmDelete(order) {
    this.confirmationPopupObject = {
      'msg': 'Are you sure you want to delete this Order?',
      'cancel': 'Cancel', 'confirm': 'Delete'
    };
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.scrollStrategy = this.overlay.scrollStrategies.noop();
    dialogConfig.data = this.confirmationPopupObject;
    const modalDialog = this.matDialog.open(ConfirmationPopupComponent, dialogConfig);
    modalDialog.afterClosed().subscribe(data => {
      if (data) {
        this.deleteOrder(order);
      }
    });
  }

  /**
   * This method used to delete the order by also deleting the related order items
   *
   * @memberof OrderDetailComponent
   */
  deleteOrder(data) {
    this.ngxService.start();
    this.orderService.removeOrder(data.id).then(res => {
      this.toastr.success('Order has been Deleted succesfully!');
      this.refreshRecords();
    }).catch(err => {
      this.ngxService.stop();
      this.toastr.error(err);
    });
  }

  /**
   * Method that opens the Add a product popup
   *
   * @memberof OrderDetailComponent
   */
  openAddVendorPopup(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.scrollStrategy = this.overlay.scrollStrategies.noop();
    dialogConfig.id = 'modal-component';
    dialogConfig.height = '80vh';
    dialogConfig.width = 'auto';
    dialogConfig.data = {'function': this.refreshRecords.bind(this),
                         'orderData': data};
    const modalDialog = this.matDialog.open(AddVendorPopupComponent, dialogConfig);
  }

  /**
   * Method that opens the Add a product popup
   *
   * @memberof OrderDetailComponent
   */
  openInvoicePopup() {
    if (this.orders.length <= 0) {
      this.toastr.warning('There are no Orders');
    } else {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = false;
      dialogConfig.scrollStrategy = this.overlay.scrollStrategies.noop();
      dialogConfig.id = 'modal-component';
      dialogConfig.height = '80vh';
      dialogConfig.width = 'max-content';
      dialogConfig.data = {'orders': this.orders, 'date': this.orders[0].date};
      const modalDialog = this.matDialog.open(ViewInvoicePopupComponent, dialogConfig);
    }
  }

  dateChanged(event) {
    this.selectedDate = formatDate(event.args.date, 'yyyy/MM/dd', 'en');
    this.isDateChanged = true;
    this.dataTable.DataTable().clear().destroy();
    this.isTimeOut = false;
    this.rerenderTable();
    this.getOrders();
  }

}
