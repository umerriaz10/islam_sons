import { Component, OnInit, ViewChild } from '@angular/core';
import { InventoryRecord } from 'src/app/interfaces/inventory-record';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { InventoryRecordService } from 'src/app/services/inventory-record/inventory-record.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-responsive';
import { ViewInventoryRecPopupComponent } from 'src/app/components/view-inventory-rec-popup/view-inventory-rec-popup.component';
import { InventoryRecordItems } from 'src/app/interfaces/inventory-record-items';

/**
 * This component is responsible for manipulating the inventory history data.
 */
@Component({
  selector: 'app-inventory-history',
  templateUrl: './inventory-history.component.html',
  styleUrls: ['./inventory-history.component.scss']
})
export class InventoryHistoryComponent implements OnInit {

  /**
   * This array stores all the timeLogs of a particular freelancer's timeTrack
   *
   * @type {Array<InventoryRecord>}
   * @memberof InventoryHistoryComponent
   */
  inventoryRecords: Array<InventoryRecord> = [];

  /**
   * Holds the records of inventory
   *
   * @type {Array<any>}
   * @memberof InventoryHistoryComponent
   */
  records: Array<any> = [];

  @ViewChild('dataTable', { static: true }) table;

  /**
   * Holds the data Table
   *
   * @type {*}
   * @memberof InventoryHistoryComponent
   */
  dataTable: any;

  /**
   * Holds the dataTable options object
   *
   * @memberof InventoryHistoryComponent
   */
  dtOption = {
    'paging':   false,
    'ordering': true,
    'info':     false,
    'responsive': {
      'details': true,
      'type': 'inline'
    }
  };

  /**
   * Holds the row of the data Table
   *
   * @memberof InventoryHistoryComponent
   */
  tableRow;

  /**
   * Holds the inventory records items data
   *
   * @type {Array<InventoryRecordItems>}
   * @memberof InventoryHistoryComponent
   */
  invRecItems: Array<InventoryRecordItems>;

  /**
   * Creates an instance of InventoryHistoryComponent.
   *
   * @param {NgxUiLoaderService} ngxService
   * @param {InventoryRecordService} inventoryRecordService
   * @param {ToastrService} toastr
   * @param {MatDialog} matDialog
   * @param {InventoryRecordService} invRecService
   * @memberof InventoryHistoryComponent
   */
  constructor(
    private ngxService: NgxUiLoaderService,
    private inventoryRecordService: InventoryRecordService,
    private toastr: ToastrService,
    public matDialog: MatDialog,
    private invRecService: InventoryRecordService
  ) { }

  /**
   * This method is triggered when the component is initialized and get all the records
   *
   * @memberof InventoryHistoryComponent
   */
  ngOnInit() {
    this.getAllInventoryRecords();
  }

  /**
   * Get all the inventory records from firestore
   *
   * @memberof InventoryHistoryComponent
   */
  getAllInventoryRecords() {
    this.ngxService.start();
    this.dataTable = $(this.table.nativeElement);
    this.dataTable.DataTable().destroy();
    this.inventoryRecords = [];
    this.records = [];
    const invRec = this.invRecService.getInvRecordsFromLocal();
    if (invRec === undefined || invRec === null) {
    this.inventoryRecordService.getInventoryRecords().subscribe(res => {
      // if (res.data) {
        res.forEach(element => {
          const pro = element.data();
          pro.id = element.id;
          this.records.push(pro);
        });
        this.records.forEach(rec => {
          this.inventoryRecords.push({
            'id': rec.id,
            'date': rec.date,
            'no_of_products': rec.no_of_products,
            'status': rec.status,
            'addedBy': rec.addedBy,
            'invRecItems': rec.invRecItems
          });
        });
        this.inventoryRecordService.saveInvRecordsLocalStorage(this.inventoryRecords);
        this.setDatable();
      // }
    }, err => {
      this.toastr.error(err);
      this.ngxService.stop();
    });
  } else {
    this.inventoryRecords = invRec;
    this.setDatable();
  }
  }

  /**
   * Method to be called on the click of refresh button
   *
   * @memberof InventoryHistoryComponent
   */
  refreshRecords () {
    this.invRecService.saveInvRecordsLocalStorage(null);
    this.getAllInventoryRecords();
  }

  /**
   * Method that sets the dataTable
   *
   * @memberof InventoryHistoryComponent
   */
  setDatable() {
    this.tableRow = this.dataTable.DataTable({
      dtOption: this.dtOption,
      data: this.inventoryRecords,
      columns: [
        { 'data': 'date' },
        { 'data': 'no_of_products' },
        { 'data': 'status' },
        { 'data': 'addedBy' },
      ],
      language: {
        paginate: {
          previous: 'Prev'
        },
        searchPlaceholder: 'Search',
        lengthMenu: 'Display _MENU_ records per page <br><i class="fa fa-exclamation-circle"></i>' +
        ' Click on the row to see the Details of the desired record',
      }});
      this.viewInvRecord();
      this.ngxService.stop();
  }

  /**
   * Method that sets the behaviour of the dataTable row
   *
   * @memberof InventoryHistoryComponent
   */
  viewInvRecord() {
    const self = this;
   $(self.dataTable).on( 'click', 'tr', function () {
     const data = self.tableRow.row(this).data();
     self.invRecItems = data.invRecItems;
     self.openInventoryRecPopup(data);
 });
 }

 /**
  * Method that opens the popup to display inventory records
  *
  * @param {*} data
  * @memberof InventoryHistoryComponent
  */
 openInventoryRecPopup(data) {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = false;
  dialogConfig.id = 'modal-component';
  dialogConfig.height = 'auto';
  dialogConfig.width = 'auto';
  dialogConfig.data = {'data': data, 'products': this.invRecItems};
  const modalDialog = this.matDialog.open(ViewInventoryRecPopupComponent, dialogConfig);
 }

}
