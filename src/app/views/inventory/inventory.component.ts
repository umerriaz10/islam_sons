import { Component, OnInit } from '@angular/core';
import { Items } from 'src/app/interfaces/items';
import { ItemsService } from 'src/app/services/items/items.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  /**
   * Holds the items coming from firestore
   *
   * @type {Array<Items>}
   * @memberof DashboardMainComponent
   */
  items: Array<Items> = [];

  /**
   * Creates an instance of DashboardMainComponent.
   *
   * @param {ItemsService} itemService
   * @memberof DashboardMainComponent
   */
  constructor(
    private itemService: ItemsService,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService
  ) { }

  /**
   * This is the hook called on the initialization of the component, it initializes
   * the form.
   *
   * @memberof DashboardMainComponent
   */
  ngOnInit() {
    this.ngxService.start();
    const items = this.itemService.getItemsFromLocal();
    if (items === null || items === undefined ) {
      this.items = [];
      this.itemService.getItems().subscribe(res => {
        res.forEach(product => {
          const pro = product.data();
          pro.id = product.id;
          this.items.push(pro);
        });
        this.itemService.saveItemsLocalStorage(this.items);
        this.ngxService.stop();
      }, err => {
        this.toastr.error(err);
        this.ngxService.stop();
      });
    } else {
      this.items = items;
      this.ngxService.stop();
    }
  }

  /**
   * Updates the item for other tabs
   *
   * @memberof DashboardMainComponent
   */
  updateItem() {
    this.ngxService.start();
    this.items = [];
    this.itemService.getItems().subscribe(res => {
      res.forEach(product => {
        const pro = product.data();
        pro.id = product.id;
        this.items.push(pro);
      });
      this.itemService.saveItemsLocalStorage(this.items);
      this.ngxService.stop();
    }, err => {
      this.toastr.error(err);
      this.ngxService.stop();
    });
  }

}
