import { Component } from '@angular/core';
import { CustomBreadcrumbs } from 'src/app/interfaces/custom-breadcrumbs';
import { CustomBreadcrumbsService } from 'src/app/services/custom-breadcrumbs/custom-breadcrumbs.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

/**
 * This component is rendered on the initialization of the application.
 */
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  /**
   * This variable holds the current url
   */
  route: string;

  /**
   * This variable holds the parsed breadcrumbs string
   */
  breadcrumbs: string;

  /**
   * This variable has the returned values after custom breadcrumbs generation
   *
   * @type {Array<CustomBreadcrumbs>}
   * @memberof MainComponent
   */
  cb: Array<CustomBreadcrumbs> = [];

  /**
   * This variable is used to store the name of the component that the user is using currently.
   *
   * @type {string}
   * @memberof MainComponent
   */
  componentName: string;

  /**
   * Class Constructor is used to inject Router, Location, and CustomBreadcrumbsService in the component.
   *
   * @param router
   * @param location
   * @param customBreadcrumb
   */
  constructor(
    private router: Router,
    private location: Location,
    private customBreadcrumb: CustomBreadcrumbsService) {
    this.getBreadcrumbs();
  }

  /**
   * Gets the current url from the Router service and sends it to CustomBreadcrumbsService to display route.
   * @memberof MainComponent
   */
  getBreadcrumbs() {
    this.router.events.subscribe(() => {
      if (this.location.path() !== '') {
          const route = this.location.path();
          this.route = this.updateDasboardRoute(route);
          this.cb = this.customBreadcrumb.parseRoute(this.route);
          this.cb.reverse();
      } else {
        this.route = 'Dashboard';
        this.breadcrumbs = this.route;
      }
    });
  }

  /**
   * This method updates the route by removing the required attributes from the routes.
   *
   * @param {string} route
   * @returns {string}
   * @memberof MainComponent
   */
  updateDasboardRoute(route: string): string {
    if (route.includes('order-detail') || route.includes('product-detail') || route.includes('vendor-detail')) {
      const splitted_route = route.split('/');
      route = splitted_route[2] + '/' + splitted_route[4];
    }
    if (route.includes('view-orders/Daily')) {
      route = 'dashboard/daily-orders';
    } else if (route.includes('view-orders/All')) {
      route = 'dashboard/all-orders';
    } else if (route.includes('new-order')) {
      route = 'dashboard/new-order';
    } else if (route.includes('view-truck-expenses/All')) {
      route = 'dashboard/all-mazda-expenses';
    } else if (route.includes('view-truck-expenses/Daily')) {
      route = 'dashboard/mazda-expenses';
    } else if (route.includes('new-truck-expense')) {
      route = 'dashboard/new-expense';
    } else if (route.includes('dashboard')) {
      route = '';
    }
    return route;
  }
}
