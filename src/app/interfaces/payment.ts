export interface Payment {
    id?: string;
    date?: Date;
    vendor_name?: string;
    vendor_id?: string;
    order_id?: string;
    amount?: number;
    pay?: boolean;
    receive?: boolean;
    addedBy?: string;
}
