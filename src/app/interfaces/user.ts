export interface User {
    CNIC?: number;
    name?: string;
    contactNumber?: number;
    role?: string;
}
