export interface TruckExpense {
    id?: string;
    date?: string;
    custom_date?: Date;
    mazda_no?: string;
    driver_name?: string;
    addedBy?: string;
    description?: number;
    expense?: number;
}
