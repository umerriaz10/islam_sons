/**
 * This interface is used to create vendor's model.
 *
 * @export
 * @interface Vendors
 */
export interface Vendors {
    id?: string;
    v_name?: string;
    v_email?: string;
    v_contact_number?: number;
    v_address?: string;
    v_balance?: number;
    v_totalPaid?: number;
    v_paidToVendor?: number;
    v_addedBy?: string;
    v_modifiedBy?: string;
    status?: string;
}
