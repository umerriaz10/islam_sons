export interface OrderItmes {
    order_id?: string;
    item_name?: string;
    item_id?: string;
    quantity?: number;
    item_price?: number;
    total?: number;
    new_id?: number;
}
