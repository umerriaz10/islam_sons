/**
 * @ignore
 */
interface NavAttributes {
    [propName: string]: any;
  }

  /**
   * @ignore
   */
interface NavWrapper {
    attributes: NavAttributes;
    element: string;
  }

  /**
   * @ignore
   */
interface NavBadge {
    text: string;
    variant: string;
  }

  /**
   * @ignore
   */
interface NavLabel {
    class?: string;
    variant: string;
  }

  /**
   * @ignore
   */
export interface NavData {
    name?: string;
    url?: string;
    icon?: string;
    badge?: NavBadge;
    title?: boolean;
    children?: NavData[];
    variant?: string;
    attributes?: NavAttributes;
    divider?: boolean;
    class?: string;
    label?: NavLabel;
    wrapper?: NavWrapper;
  }

  /**
   * @ignore
   */
export const navItems: NavData[] = [
    {
      name: 'Dashboard',
      url: '/main/kanban-board',
      icon: 'icon-speedometer',
    },
    {
      name: 'Orders',
      url: '/order',
      icon: 'fa fa-shopping-cart',
      children: [
        {
          name: 'New Order',
          url: '/main/new-order',
          icon: 'fa fa-plus',
        },
        {
          name: 'View Order',
          url: '/main/view-orders',
          icon: 'fa fa-eye',
        }
      ]
    },
    {
      name: 'Payments',
      url: '/payment',
      icon: 'fa fa-credit-card',
      children: [
        {
          name: 'New Payment',
          url: '/main/new-payment',
          icon: 'fa fa-plus',
        },
        {
          name: 'View Payments',
          url: '/main/payments',
          icon: 'fa fa-eye',
        }
      ]
    },
    {
      name: 'Vendors',
      url: '/main/view-vendors',
      icon: 'fa fa-user',
    },
    {
      name: 'Products',
      url: '/main/products',
      icon: 'fa fa-product-hunt',
    },
    {
      name: 'Inventory',
      url: '/inventories',
      icon: 'fa fa-table',
      children: [
        {
          name: 'Update Inventory',
          url: '/main/inventory',
          icon: 'fa fa-plus',
        },
        {
          name: 'Inventory Records',
          url: '/main/inventory-records',
          icon: 'fa fa-eye',
        }
      ]
    },
    {
      name: 'Company Summary',
      url: '/main/company-summary',
      icon: 'fa fa-file-text',
    },
    {
      name: 'Logout',
      url: 'logout',
      icon: 'icon-logout',
    }
  ];
