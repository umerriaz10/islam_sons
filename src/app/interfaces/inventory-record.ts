import { InventoryRecordItems } from './inventory-record-items';

/**
 * This interface is used to store inverntory records.
 *
 * @export
 * @interface InvertoryRecord
 */

export interface InventoryRecord {
    id?: string;
    no_of_products?: number;
    date?: Date;
    status?: string;
    addedBy?: string;
    invRecItems?: Array<InventoryRecordItems>;
}
