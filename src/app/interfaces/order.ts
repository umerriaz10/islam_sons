import { OrderItmes } from './order-itmes';

/**
 * This holds the model of database order
 *
 * @export
 * @interface Order
 */
export interface Order {
    id?: string;
    press_name?: string;
    date?: Date;
    custom_date?: Date;
    mill_name?: string;
    mazda_no?: string;
    driver_name?: string;
    orderedBy?: string;
    labour?: string;
    bbc?: number;
    btn?: number;
    bleach?: number;
    carug?: number;
    news?: number;
    wc?: number;
    proof?: number;
    shine?: number;
    weightInPress?: number;
    weightInMill?: number;
    // modifiedBy?: string;
    // total_amount?: number;
    // orderItems?: Array<OrderItmes>;
    // vendor_id?: string;
    // no_of_products?: number;
}
