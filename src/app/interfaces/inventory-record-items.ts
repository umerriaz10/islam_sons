/**
 * This interface is used to store inverntory records.
 *
 * @export
 * @interface InvertoryRecordItems
 */

export interface InventoryRecordItems {
    item_name?: Date;
    quantity?: string;
}
