/**
 * This interface is used to create item's model.
 *
 * @export
 * @interface Items
 */
export interface Items {
    id?: string;
    p_name?: string;
    p_quantity?: number;
    p_price?: number;
    p_addedBy?: string;
}
