import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdapterService } from '../adapter/adapter.service';
import { AngularFirestore } from '@angular/fire/firestore';


/**
 * Service that have the required functions for records module.
 *
 * @export
 * @class InventoryRecordService
 */
@Injectable({
  providedIn: 'root'
})
export class InventoryRecordService {


  constructor(
    private adapter: AdapterService,
    private firestore: AngularFirestore
  ) { }

  /**
   * Stores the record of inventory in firestore
   *
   * @param {*} record
   * @memberof InventoryRecordService
   */
  postInventoryRecord(record) {
    return this.firestore.collection('/inventoryRecord').add(record);
  }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  getInventoryRecords(): Observable<any> {
    return this.firestore.collection('/inventoryRecord').get();
  }

  // /**
  //  * Stores the record of inventory in firestore
  //  *
  //  * @param {*} record
  //  * @memberof InventoryRecordService
  //  */
  // postInventoryRecordItems(record) {
  //   return this.adapter.post('/inventoryRecordItems', record);
  // }

  // /**
  //  * Get the details of the inventory records
  //  *
  //  * @memberof InventoryRecordService
  //  */
  // getInventoryRecords(): Observable<any> {
  //   return this.adapter.get('/inventoryRecord', null, null);
  // }

  // /**
  //  * Stores the record of inventory in firestore
  //  *
  //  * @param {*} record
  //  * @memberof InventoryRecordService
  //  */
  // getInventoryRecordItemsById(id) {
  //   return this.adapter.get('/inventoryRecordItems/' + id, null, null);
  // }

  /**
   * Stores the vendors in local storage
   *
   * @param {*} vendors
   * @memberof VendorsService
   */
  saveInvRecordsLocalStorage(invRecords) {
    window.localStorage.setItem('invRecords', JSON.stringify(invRecords));
  }

  /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getInvRecordsFromLocal() {
    const invRecords = JSON.parse(window.localStorage.getItem('invRecords'));
    if (invRecords !== undefined && invRecords !== null) {
      return invRecords;
    } else {
      return null;
    }
  }
}
