import { Injectable } from '@angular/core';
import { CustomBreadcrumbs } from 'src/app/interfaces/custom-breadcrumbs';

/**
 * Service used to parse a given route and generate breadcrumbs.
 */
@Injectable({
  providedIn: 'root'
})
export class CustomBreadcrumbsService {
  /**
   * This method is used to parse a given route and return breadcrumbs.
   *
   * The method accepts the current url of the portal, and replaces all unneccassary characters from the provided url, and
   * parses the url in multiple substrings.
   *
   * @param {string} route string with current route url
   */
  parseRoute(route: string) {
    const routesArray  = route;
    route = route.replace(/%3B/g, ';').replace('/main/', '').replace(/%20/g, '-').replace(/\//g, ' ').replace(/%28/g, '(')
      .replace(/%29/g, ')').replace(/%22/g, '"').replace(/%23/g, '#').replace(/%21/g, '!').replace(/%24/g, '$')
      .replace(/%26/g, '&').replace(/%3A/g, ':').replace(/%27/g, `'`).replace(/%5B/g, '[').replace(/%5D/g, ']');
    const splitRoute = route.toLowerCase().split(' ');
    const routeElements = [];
    splitRoute.forEach((elem) => {
      elem = elem.charAt(0).toUpperCase() + elem.substring(1);
      if (!this.isDate(elem)) {
        if (elem.includes('-')) {
          const dashString = elem.split('-');
          elem = '';
          dashString.forEach(dashElem => {
            dashElem = dashElem.charAt(0).toUpperCase() + dashElem.substring(1);
            elem = elem + dashElem + ' ';
          });
          elem = elem.substr(0, elem.lastIndexOf(' '));
        }
      }
      routeElements.push(elem);
    });
    return this.generateRoutes(routeElements, routesArray);
  }

  /**
   * This method takes in an array of substrings and a string containing the current url the user is on.
   *
   * The substrings are reversed, and links are assinged to them from bottom to top.
   *
   * @param {string[]} splitRoute Array of strings containing breadcrumbs
   * @param {string} routesArray string containing entire url of current route
   * @returns
   * @memberof CustomBreadcrumbsService
   */
  generateRoutes(splitRoute: string[], routesArray: string) {
    const customBreadcrumbs: Array<CustomBreadcrumbs> = [];
    let currentRoute = '';
    splitRoute.reverse();
    splitRoute.forEach(crumb => {
      const breadcrumb: CustomBreadcrumbs = {
        title: crumb,
        link: currentRoute
      };
      currentRoute = routesArray = routesArray.substr(0, routesArray.lastIndexOf('/'));
      currentRoute = routesArray;
      customBreadcrumbs.push((breadcrumb));
    });
    return customBreadcrumbs;
  }

  /**
   * method to check whether the passed argument is a date or not
   *
   * @param {*} date
   * @returns
   * @memberof CustomBreadcrumbsService
   */
  isDate(date) {
    const dt = new Date(date);
    if (isNaN(dt.getTime())) {
      return false;
    } else {
      return true;
    }
  }
}
