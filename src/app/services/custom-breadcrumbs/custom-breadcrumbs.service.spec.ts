import { TestBed } from '@angular/core/testing';

import { CustomBreadcrumbsService } from './custom-breadcrumbs.service';

describe('CustomBreadcrumbsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomBreadcrumbsService = TestBed.get(CustomBreadcrumbsService);
    expect(service).toBeTruthy();
  });
});
