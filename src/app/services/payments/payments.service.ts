import { Injectable } from '@angular/core';
import { AdapterService } from '../adapter/adapter.service';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  constructor(
    private adapter: AdapterService,
    private firestore: AngularFirestore
  ) { }

  /**
   * post a new vendor in the DB
   *
   * @memberof VendorsService
   */
  postPayment(payment) {
    return this.firestore.collection('/payments').add(payment);
  }

   /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  getPayments(): Observable<any> {
    return this.firestore.collection('/payments').get();
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getPaymentsById(id): Observable<any> {
    return this.firestore.collection('/payments').doc(id).get();
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getPaymentsByVendorId(id): Observable<any> {
    return this.firestore.collection('/payments', ref => ref.where('vendor_id', '==', id)).get();
  }


  removePayment(payId) {
    return this.firestore.collection('/payments').doc(payId).delete();
  }

  /**
   * Stores the vendors in local storage
   *
   * @param {*} vendors
   * @memberof VendorsService
   */
  savePaymentsLocalStorage(payments) {
    window.localStorage.setItem('payments', JSON.stringify(payments));
  }

  /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getPaymentsFromLocal() {
    const payments = JSON.parse(window.localStorage.getItem('payments'));
    if (payments !== undefined && payments !== null) {
      return payments;
    } else {
      return null;
    }
  }
}
