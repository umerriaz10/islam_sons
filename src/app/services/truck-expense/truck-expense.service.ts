import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TruckExpenseService {

/**
   * Creates an instance of ItemsService.
   *
   * @param {ToastrService} toastr
   * @memberof ItemsService
   */
  constructor(
    private firestore: AngularFirestore
  ) { }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  postTruckExpense(truckExpense) {
    return this.firestore.collection('/truckExpenses').add(truckExpense);
  }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  getTruckExpenses(): Observable<any> {
    return this.firestore.collection('/truckExpenses').get();
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getTruckExpenseById(id): Observable<any> {
    return this.firestore.collection('/truckExpenses').doc(id).get();
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getOrdersByVendorId(id): Observable<any> {
    return this.firestore.collection('/orders', ref => ref.where('vendor_id', '==', id)).get();
  }

   /**
   * Stores the vendors in local storage
   *
   * @param {*} vendors
   * @memberof VendorsService
   */
  saveTruckExpensesLocalStorage(truckExpenses) {
    window.localStorage.setItem('truckExpenses', JSON.stringify(truckExpenses));
  }

  removeTruckExpense(teId) {
    return this.firestore.collection('/truckExpenses').doc(teId).delete();
  }

  /**
   * Method to update the item from the firestore
   *
   * @param {string} id
   * @param {*} value
   * @memberof ItemsService
   */
  updateTruckExpense(truckExpense, teId) {
    return this.firestore.collection('/truckExpenses').doc(teId).update(truckExpense);
  }

   /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getTruckExpensesFromLocal() {
    const truckExpenses = JSON.parse(window.localStorage.getItem('truckExpenses'));
    if (truckExpenses !== undefined && truckExpenses !== null) {
      return truckExpenses;
    } else {
      return null;
    }
  }
}
