import { TestBed } from '@angular/core/testing';

import { TruckExpenseService } from './truck-expense.service';

describe('TruckExpenseService', () => {
  let service: TruckExpenseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TruckExpenseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
