import { Injectable } from '@angular/core';
import { Vendors } from 'src/app/interfaces/vendors';
import { AdapterService } from '../adapter/adapter.service';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class VendorsService {

  /**
   * Holds the items coming from firestore
   *
   * @type {Array<Vendors>}
   * @memberof VendorsService
   */
  vendors: Array<Vendors> = [];

  /**
   * Creates an instance of VendorsService.
   *
   * @param {ToastrService} toastr
   * @memberof VendorsService
   */
  constructor(
    private adapter: AdapterService,
    private firestore: AngularFirestore
  ) { }

  /**
   * Stores the vendors in local storage
   *
   * @param {*} vendors
   * @memberof VendorsService
   */
  saveVendorsLocalStorage(vendors) {
    window.localStorage.setItem('vendors', JSON.stringify(vendors));
  }

  /**
   * Get the details of the vendors
   *
   * @memberof VendorsService
   */
  getVendors(): Observable<any> {
    return this.firestore.collection('/vendors').get();
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getVendorById(id): Observable<any> {
    return this.firestore.collection('/vendors').doc(id).get();
  }

  /**
   * post a new vendor in the DB
   *
   * @memberof VendorsService
   */
  postVendor(vendor) {
    return this.firestore.collection('/vendors').add(vendor);
  }

  updateVendorBalance(data) {
    if (data.status === 'Added') {
      return this.firestore.collection('/vendors').doc(data.id).update({
        v_balance: firebase.firestore.FieldValue.increment(data.v_balance)
      });
    } else {
      return this.firestore.collection('/vendors').doc(data.id).update({
        v_balance: firebase.firestore.FieldValue.increment(-data.v_balance)
      });
    }
  }

  /**
   * Method to update the item from the firestore
   *
   * @param {string} id
   * @param {*} value
   * @memberof ItemsService
   */
  updateVendor(vendor, vId) {
    return this.firestore.collection('/vendors').doc(vId).update(vendor);
  }

  /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getVendorsFromLocal() {
    const vendors = JSON.parse(window.localStorage.getItem('vendors'));
    if (vendors !== undefined && vendors !== null) {
      return vendors;
    } else {
      return null;
    }
  }
}
