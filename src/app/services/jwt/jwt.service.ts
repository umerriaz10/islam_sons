import { Injectable } from '@angular/core';

/**
 * Injects the JSON web token service in the project
 *
 * This service is responsible for management of the jwt token, which would be used
 * for authentication/authorization purposes. The service includes methods to save, delete
 * and retrieve the token
 */
@Injectable({
  providedIn: 'root'
})

export class JwtService {
  /**
   * This constructor initializes the required dependencies NGXLogger and TranslatePipe
   */
  constructor() { }

  /**
   * This method fetches the token from local storage and returns it.
   *
   * @method getToken
   * @return
   */
  getToken(): string {
    // tslint:disable-next-line: no-string-literal
    const token = window.localStorage['token'];
    if (token !== undefined) {
      return token;
    } else {
      return null;
    }
  }

  /**
   * This method takes the token and saves it into the local storage.
   *
   *
   * @param  token
   * @method saveToken
   * @return
   */
  saveToken(token) {
    if (token === undefined) {
      throw new Error('generic[responses][error][doc][002]');
    } else {
      try {
        window.localStorage.setItem('token', token);
      } catch (err) {
        throw new Error(err);
      }
    }
  }

  /**
   * This method deletes the token from local Storage.
   *
   * @method destroyToken
   * @return
   */
  destroyToken() {
    if (this.getToken() !== undefined) {
      window.localStorage.removeItem('token');
    }
  }

  /**
   * This method is used to check for expired token.
   *
   * First, check if the token expiry date exists in local storage or
   * not. If not, user will be redirected to the login page.
   *
   * If the token expiry date exists, the current date will be compared to it. If its equal or higher than the
   * expiry date, user will be redirected to login page again.
   *
   * If the token expiry date passes both tests, the method returns false, indicating that the token has not
   * yet expired.
   *
   * @returns
   * @memberof JwtService
   */
  checkForExpiredToken() {
    // tslint:disable-next-line: one-variable-per-declaration
    const date: Date = new Date(window.localStorage.token_expiry_date),
    today = new Date(window.localStorage.current_date);
    if (today.getTime() >= date.getTime()) {
      this.destroyToken();
      return true;
    }
    return false;
  }
}
