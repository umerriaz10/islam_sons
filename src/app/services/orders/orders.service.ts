import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  /**
   * Creates an instance of ItemsService.
   *
   * @param {ToastrService} toastr
   * @memberof ItemsService
   */
  constructor(
    private firestore: AngularFirestore
  ) { }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  postOrder(order) {
    return this.firestore.collection('/orders').add(order);
  }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  getOrders(): Observable<any> {
    return this.firestore.collection('/orders').get();
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getOrderById(id): Observable<any> {
    return this.firestore.collection('/orders').doc(id).get();
  }

  /**
   * Get a specific order by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getOrdersByVendorId(id): Observable<any> {
    return this.firestore.collection('/orders', ref => ref.where('vendor_id', '==', id)).get();
  }

   /**
   * Stores the vendors in local storage
   *
   * @param {*} vendors
   * @memberof VendorsService
   */
  saveOrdersLocalStorage(orders) {
    window.localStorage.setItem('orders', JSON.stringify(orders));
  }

  removeOrder(oId) {
    return this.firestore.collection('/orders').doc(oId).delete();
  }

  /**
   * Method to update the item from the firestore
   *
   * @param {string} id
   * @param {*} value
   * @memberof ItemsService
   */
  updateOrder(order, oId) {
    return this.firestore.collection('/orders').doc(oId).update(order);
  }

   /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getOrdersFromLocal() {
    const orders = JSON.parse(window.localStorage.getItem('orders'));
    if (orders !== undefined && orders !== null) {
      return orders;
    } else {
      return null;
    }
  }
}
