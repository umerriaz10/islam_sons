import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  HttpHeaders,
  HttpClient,
  HttpParams,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { JwtService } from 'src/app/services/jwt/jwt.service';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';

/**
* This variable contains the URL of the API fetched from the environment configuration.
// */
// const API = environment.firebaseConfig;

/**
* This Service provides the basic functionality required to interact with the API i.e. GET, POST,
* PATCH, DELETE.
*/
@Injectable({
  providedIn: 'root'
})

export class AdapterService {
  /**
  * The Constructor initializes the HttpClient, jwtService & LoggerService in the class.
  */
  constructor(private http: HttpClient,
    private jwtService: JwtService,
    private router: Router) { }
  /**
   * This method logs the errors if the API call fails.
   *
   * @param self
   * @method formatErrors
   * @return
   */
  private formatErrors(error: any) {
    if (error.error.error === 'invalid_grant') {
      this.jwtService.destroyToken();
      this.router.navigate(['']);
    }
    return throwError(error.error);
  }

  /**
   * This method adds the headers to the API requests.
   *
   * If the request is made for uplaoding documents, the isDocument parameter is used to
   * set up 'enctype', else we use 'content-type' for all other requests.
   *
   * @param headerData
   * @param isDocument
   * @method requestHeaders
   * @return
   */
  private requestHeaders(headerData: any, isDocument = false): HttpHeaders {
    let headers = new HttpHeaders(
    );
    headers = (!isDocument) ? headers.append('content-type', 'application/json') : headers.append('enctype', 'multipart/form-data');
    if (this.jwtService.getToken() !== null) {
      headers = headers.append( 'authorization', this.jwtService.getToken());
    }
    if (headerData !== null && headerData.length !== 0) {
      Object.keys(headerData).forEach(function (key) {
        headers = headers.set(key, headerData[key]);
      });
    }
    return headers;
  }

  /**
   * This method generates the GET api call. If the request is related to registration form, the headers parameter will
   * contain null, in order to avoid checking for existence of a token.
   *
   * @param path
   * @param headers
   * @param params
   * @method get
   * @return
   */
  get(path: string, data: any, headers: any, params: HttpParams = new HttpParams()): Observable<any> {
    const header = this.requestHeaders(data);
    return this.http.get(`${path}`, { headers: header, params: params })
      .pipe(catchError(this.formatErrors.bind(this)));
  }

  /**
   * This method generates the PUT api call.
   *
   * @param path
   * @param body
   * @method put
   * @return
   */
  patch(path: string, body: Object = {}, headers = null): Observable<any> {
    const header = this.requestHeaders(headers);
    return this.http.patch(
      `${path}`,
      JSON.stringify(body),
      { headers: header }
    ).pipe(catchError(this.formatErrors.bind(this)));
  }


  /**
   * This method generates the POST api call.
   *
   * @param path
   * @param body
   * @method post
   * @return
   */
  post(path: string, body: Object = {}, header = null): Observable<any> {
    header = this.requestHeaders(header);
    return this.http.post(
      `${path}`,
      JSON.stringify(body),
      { headers: header }
    ).pipe(catchError(this.formatErrors.bind(this)));
  }

  /**
  * This method generates the POST api call.
  *
  * @param path
  * @param body
  * @method post
  * @return
  */
 getDocument(path: string): Observable<any> {
  return this.http.post(
     `${path}`,
     null,
     {
       responseType: 'blob'
      }
   ).pipe(catchError(this.formatErrors.bind(this)));
 }

  /**
   * This method generates the DELETE api call.
   *
   * @param path
   * @method delete
   * @return
   */
  delete(path, headers = null): Observable<any> {
    const header = this.requestHeaders(headers);
    // const params: HttpParams = new HttpParams();
    return this.http.delete(
      `${path}`, { headers: header }
    ).pipe(catchError(this.formatErrors.bind(this)));
  }
}
