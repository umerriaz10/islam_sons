import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Items } from 'src/app/interfaces/items';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AdapterService } from '../adapter/adapter.service';
import * as firebase from 'firebase';

/**
 * Service that have the required functions for the items module.
 *
 * @export
 * @class ItemsService
 */
@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  /**
   * Creates an instance of ItemsService.
   *
   * @param {ToastrService} toastr
   * @memberof ItemsService
   */
  constructor(
    private adapter: AdapterService,
    private toastr: ToastrService,
    private firestore: AngularFirestore
  ) { }

  /**
   * Save the products in local storage
   *
   * @memberof ItemsService
   */
  saveItemsLocalStorage(items) {
    window.localStorage.setItem('items', JSON.stringify(items));
  }

  /**
   * Gets the items from local storage
   *
   * @returns
   * @memberof ItemsService
   */
  getItemsFromLocal() {
    const items = window.localStorage.getItem('items');
    if (items !== 'undefined' && items !== null) {
      return JSON.parse(items);
    } else {
      return null;
    }
  }

  /**
   * Get the details of the items
   *
   * @memberof ItemsService
   */
  getItems(): Observable<any> {
    return this.firestore.collection('products').get();
  }

  /**
   * Get a specific product by id
   *
   * @param {*} id
   * @returns {Observable<any>}
   * @memberof OrdersService
   */
  getProductById(id): Observable<any> {
    return this.firestore.collection('/products').doc(id).get();
  }

  /**
   * Post the item in the DB
   *
   * @memberof ItemsService
   */
  postItem(item) {
    return this.firestore.collection('products').add(item);
  }

  /**
   * Method to update the item from the DB
   *
   * @param {string} id
   * @param {*} value
   * @memberof ItemsService
   */
  updateItemQuantity(id: string, value, status) {
    if (status === 'Added') {
      return this.firestore.collection('/products').doc(id).update({
        p_quantity: firebase.firestore.FieldValue.increment(value)
      });
    } else {
      return this.firestore.collection('/products').doc(id).update({
        p_quantity: firebase.firestore.FieldValue.increment(-value)
      });
    }
  }

  /**
   * Method to update the item from the DB
   *
   * @param {string} id
   * @param {*} value
   * @memberof ItemsService
   */
  updateItem(product, pId) {
    return this.firestore.collection('/products').doc(pId).update(product);
  }

  removeProduct(pId) {
    return this.firestore.collection('/products').doc(pId).delete();
  }

}
