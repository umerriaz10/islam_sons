import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private angularFireAuth: AngularFireAuth,
    private firestore: AngularFirestore
  ) { }

  /**
   * Get the details of the user
   *
   * @param {*} id
   * @memberof UserService
   */
  login(cred) {
    return this.angularFireAuth.signInWithEmailAndPassword(cred.email, cred.password);
  }

  /**
   * Get the user after login from local storage
   *
   * @returns
   * @memberof UserService
   */
  getUserFirestore(uid) {
    return this.firestore.collection('/users').doc(uid).get();
  }

  /**
   * Save the user after login in local storage
   *
   * @param {*} user
   * @memberof UserService
   */
  saveUser(user) {
    window.localStorage.setItem('user', JSON.stringify(user));
  }

  /**
   * Get the user after login from local storage
   *
   * @returns
   * @memberof UserService
   */
  getUser() {
    const user = JSON.parse(window.localStorage['user']);
    if (user !== undefined && user !== null) {
      return user;
    } else {
      return null;
    }
  }
}
