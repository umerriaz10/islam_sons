import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalServiceService {

  /**
   * This variable is used to store the pop up for invoice (a modal).
   *
   * @memberof ModalService
   */
  public viewOrderModal: any;

 /**
   * This method is used to set the invoice modal variable.
   *
   * @param modal
   * @memberof ModalService
   */
  setViewOrderPopup(modal) {
    this.viewOrderModal = modal;
  }

  /**
   * This method is used to return the invoice modal variable.
   *
   * @returns
   * @memberof ModalService
   */
  getViewOrderPopup() {
   return this.viewOrderModal;
  }
}
